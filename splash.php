<?php

/*

	Template Name: Splash

*/

include(locate_template('partials/header/global-variables.php')); ?>

<!DOCTYPE html>
<html>
<head>
	<?php the_field('head_meta', 'options'); ?>

	<meta charset="utf-8" />
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<?php get_template_part('partials/head/styles'); ?>
	
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<?php the_field('body_meta', 'options'); ?>

    <section class="splash">
        <div class="splash-header">
            <div class="logo">
                <img src="<?php $image = get_field('logo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
            </div>
        </div>

        <div class="splash-body" style="background-image: url(<?php $image = get_field('background_image'); echo $image['url']; ?>);">
            <div class="links">
                <?php if(have_rows('buttons')): while(have_rows('buttons')): the_row(); ?>

                    <?php if(get_sub_field('disable')): ?>
                        <?php 
                            $link = get_sub_field('link');
                            if( $link ): 
                            $link_url = $link['url'];
                            $link_title = $link['title'];
                            $link_target = $link['target'] ? $link['target'] : '_self';
                        ?>

                            <div class="link">
                                <a href="#" class="disabled"><?php echo esc_html($link_title); ?><span class="message"><?php the_sub_field('message'); ?></span></a>
                                
                            </div>

                        <?php endif; ?>
                    <?php else: ?>
    
                        <?php 
                            $link = get_sub_field('link');
                            if( $link ): 
                            $link_url = $link['url'];
                            $link_title = $link['title'];
                            $link_target = $link['target'] ? $link['target'] : '_self';
                        ?>

                            <div class="link">
                                <a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                            </div>

                        <?php endif; ?>

                    <?php endif; ?>

                <?php endwhile; endif; ?>
            </div>
        </div>
    </section>

<?php include(locate_template('partials/footer/scripts.php')); ?>

<?php wp_footer(); ?>

</body>
</html>