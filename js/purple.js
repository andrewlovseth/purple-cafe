(function ($, window, document, undefined) {


	$(document).ready(function() {

		$('a[href$=".pdf"]').attr('target', '_blank');

		// Disabled
		$('a.disabled').on('click', function() {
			return false;
		});
	});

});