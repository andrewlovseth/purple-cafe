<?php

/*

	Template Name: Locations

*/

get_header(); ?>
	<section class="standard">
		<div class="wrapper">

			<div class="section-header">
				<h1>Locations</h1>
			</div>

			<?php if(have_rows('locations')): while(have_rows('locations')): the_row(); ?>

				<?php $location = get_sub_field('location'); ?>

	    		<?php if(get_field('closed', $location) !== true): ?>

				    <div class="location">

				    	<div class="photo">
				    		<a href="<?php echo get_permalink($location->ID); ?>">
				    			<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				    		</a>		    		
				    	</div>

				    	<div class="info">
				    		<div class="headline">
				    			<h3><a href="<?php echo get_permalink($location->ID); ?>"><?php echo get_the_title($location->ID); ?></a></h3>
				    		</div>

				    		<div class="copy">
				    			<?php the_sub_field('copy'); ?>
				    		</div>

				    		<div class="cta">
				    			<a href="<?php echo get_permalink($location->ID); ?>" class="btn">More Info</a>
				    		</div>
				    	</div>
				    </div>

	    		<?php else: ?>

				    <div class="location">

				    	<div class="photo">
				    		<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				    	</div>

				    	<div class="info">
				    		<div class="headline">
				    			<h3><?php echo get_the_title($location->ID); ?></h3>
				    		</div>

				    		<div class="copy">
				    			<?php the_sub_field('copy'); ?>
				    		</div>
				    	</div>
				    </div>

	    		<?php endif; ?>

	    	<?php endwhile; endif; ?>

		</div>
	</section>

<?php get_footer(); ?>