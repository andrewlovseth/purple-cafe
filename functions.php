<?php

add_filter('body_class', 'bearsmith_body_class');
function bearsmith_body_class( $classes ) {

    global $post;
    $location = bearsmith_get_location($post); 
    $classes[] = $location;
     
    return $classes;    
}