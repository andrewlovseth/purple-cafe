<?php include(locate_template('partials/header/global-variables.php')); ?>

<?php if ( $_SERVER["SERVER_ADDR"] == '127.0.0.1' ): ?>
	<link rel="stylesheet" type="text/css" media="all" href="/wp-content/themes/heavy-restaurants/heavy-restaurants.css" />
<?php else: ?>
	<link rel="stylesheet" type="text/css" media="all" href="/purple-2020/wp-content/themes/heavy-restaurants/heavy-restaurants.css" />
<?php endif; ?>

<link rel="stylesheet" type="text/css" media="all" href="<?php echo $child_theme_path; ?>/<?php echo $css_filename; ?>.css" />