<?php

    $slug = bearsmith_get_location($post);
    $header = get_field($slug . '_header', 'options');
    $logo = $header['logo'];

?>

<div class="logo">
	<div class="image">
		<a href="<?php echo '/' . $slug . '/'; ?>">
			<img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>" />
		</a>
	</div>

	<div class="mobile-menu-btn">
		<a href="#" id="toggle">
			<div class="patty"></div>
		</a>
	</div>

	<nav class="mobile">
		<?php get_template_part('partials/header/nav'); ?>
	</nav>
</div>