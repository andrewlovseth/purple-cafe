<?php $slug = bearsmith_get_location($post); if(have_rows($slug . '_navigation','options')): ?>
	<div class="main-nav">
		<?php while(have_rows($slug . '_navigation', 'options')): the_row(); ?>
 
			<a href="<?php the_sub_field('link'); ?>"><?php the_sub_field('label'); ?></a>

		<?php endwhile; ?>
	</div>
<?php endif; ?>