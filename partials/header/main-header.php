<?php

    $slug = bearsmith_get_location($post);
    $header = get_field($slug . '_header', 'options');
    $show_status_view = $header['show_status_view']
?>

<header<?php if($show_status_view == true): ?> class="status-view"<?php endif; ?>>
    <div class="wrapper">

        <?php // get_template_part('partials/header/popup'); ?>

        <div class="header-wrapper">

            <?php if(get_field($show_status_view, 'options')): ?>

                <?php get_template_part('partials/header/status-view'); ?>

            <?php else: ?>

                <?php get_template_part('partials/header/left-info'); ?>
                <?php get_template_part('partials/header/logo'); ?>
                <?php get_template_part('partials/header/right-info'); ?>

            <?php endif; ?>

        </div>

    </div>
</header>