<?php
	include(locate_template('partials/header/global-variables.php'));
	$slug = bearsmith_get_location($post);
	$social = get_field($slug . '_social', 'options');

	$facebook = $social['facebook'];
	$twitter = $social['twitter'];
	$instagram = $social['instagram'];

?>

<div class="social">
	<a href="<?php echo $facebook; ?>" class="facebook" rel="external">
		<img src="<?php echo $child_theme_path; ?>/images/facebook.svg" alt="Facebook" />
	</a>
	
	<a href="<?php echo $twitter; ?>" class="twitter" rel="external">
		<img src="<?php echo $child_theme_path; ?>/images/twitter.svg" alt="Twitter" />
	</a>
	
	<a href="<?php echo $instagram; ?>" class="instagram" rel="external">
		<img src="<?php echo $child_theme_path; ?>/images/instagram.svg" alt="Instagram" />
	</a>
</div>