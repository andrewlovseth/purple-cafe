<section class="utility-nav">
    <div class="wrapper">

    <div class="links">
        <h5><a href="/">Locations</a></h5>

        <?php
            $front = get_option('page_on_front');

            if(have_rows('buttons', $front)): while(have_rows('buttons', $front)): the_row(); ?>
            
            <?php 
                $link = get_sub_field('link');
                if( $link ): 
                $link_url = $link['url'];
                $link_title = $link['title'];
            ?>

                    <a href="<?php echo esc_url($link_url); ?>" class="<?php echo sanitize_title_with_dashes($link_title); ?>">
                        <?php echo esc_html($link_title); ?>
                    </a>

            <?php endif; ?>

        <?php endwhile; endif; ?>
    </div>
    
    </div>
</section>