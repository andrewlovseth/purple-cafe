<?php

    $slug = bearsmith_get_location($post);
    $header = get_field($slug . '_header', 'options');
	$location_header = $header['location_header'];
	$location = $header['location'];

?>

<div class="info right">
	<?php if($location_header): ?>
		<h3><?php echo $location_header; ?></h3>
	<?php else: ?>
		<h3>Location</h3>
    <?php endif; ?>
    
	<?php echo $location; ?>
</div>