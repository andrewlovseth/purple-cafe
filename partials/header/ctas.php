<?php $slug = bearsmith_get_location($post); if(have_rows($slug . '_ctas', 'options')): while(have_rows($slug . '_ctas', 'options')) : the_row(); ?>
 
    <?php if( get_row_layout() == 'button' ): ?>
  		
    		<div class="btn">
        		<a href="<?php the_sub_field('link'); ?>"<?php if(get_sub_field('external')): ?> rel="external"<?php endif; ?>>
        			<?php the_sub_field('label'); ?>
        		</a>
    		</div>
  		
    <?php endif; ?>

    <?php if( get_row_layout() == 'open_table_reservations' ): ?>

        <div class="reservations opentable btn">
            <div class="widget">
                <script type='text/javascript' src='https://www.opentable.com/widget/reservation/loader?rid=<?php the_sub_field('open_table_id'); ?>&type=button&theme=standard&iframe=false&domain=com&lang=en-US&newtab=false&ot_source=Restaurant%20website'></script>
            </div>          
        </div>

    <?php endif; ?>

    <?php if( get_row_layout() == 'text' ): ?>
    
        <div class="text">
            <p><?php the_sub_field('note'); ?></p>
        </div>
    
    <?php endif; ?>

 <?php endwhile; endif; ?>