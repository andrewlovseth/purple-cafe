<?php

$slug = bearsmith_get_location($post);
$location = get_page_by_path($slug);

if(get_field('show_announcement', $location->ID)): ?>

	<section id="announcement" class="top-banner">
		<div class="wrapper">

			<div class="announcement-wrapper">
				<?php the_field('announcement', $location->ID); ?>
			</div>

		</div>

        <style>

        <?php if(get_field('announcement_background_color', $location->ID)): ?>
            #announcement {
                background-color: <?php the_field('announcement_background_color', $location->ID); ?> !important;
                color: <?php the_field('announcement_text_color', $location->ID); ?> !important;
            }
        <?php endif; ?>

        <?php if(get_field('announcement_text_color', $location->ID)): ?>
            #announcement a,
            #announcement p {
                color: <?php the_field('announcement_text_color', $location->ID); ?> !important;
            }
        <?php endif; ?>

        </style>

    </section>

<?php endif; ?>
