<?php

    $slug = bearsmith_get_location($post);
    $header = get_field($slug . '_header', 'options');
	$hours_header = $header['hours_header'];
	$hours = $header['hours'];

?>

<div class="info left">
	<?php if($hours_header): ?>
		<h3><?php echo $hours_header; ?></h3>
	<?php else: ?>
		<h3>Hours</h3>
	<?php endif; ?>

	<?php echo $hours; ?>
</div>