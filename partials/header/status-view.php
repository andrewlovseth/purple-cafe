<?php

    $slug = bearsmith_get_location($post);
    $header = get_field($slug . '_header', 'options');
    $status_headline = $header['status_headline'];
    $status_copy = $header['status_copy'];

?>

<div class="status">

	<?php get_template_part('partials/header/logo'); ?>

	<div class="message">
		<div class="headline">
			<h2><?php echo $status_headline; ?></h2>
		</div>

		<div class="copy">
			<?php echo $status_copy; ?>
		</div>
	</div>

</div>