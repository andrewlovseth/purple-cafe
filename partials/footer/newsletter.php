<div class="actions">
	<div class="action newsletter">
		
		<h4>Sign up for our newsletter to get updates on news and events</h4>

		<div class="cta">
			<a href="#" class="btn" id="subscribe">Stay Connected</a>
		</div>

	</div>

	<div class="action comment">
		
		<h4>Have comments or suggestions?</h4>

		<div class="cta">
            <?php
				$slug = bearsmith_get_location($post);
				$email = get_field($slug . '_email', 'options');
			?>
			<a href="mailto:<?php echo $email ?>" class="btn">Drop Us a Line</a>
		</div>

	</div>
</div>